{-|
Module      : Question Set 3
Author      : Questions written by Joshua Corner
Credit      : Some inspired by COMP1100 labs
Purpose     : Preparation for mid-semester exam and practice of Haskell basics
-}
module QuestionSet3 where

import Data.List hiding (subsequences)

-- | ========================= Question 1 =============================== | --
-- | PATTERN MATCHING AND CASE STATEMENTS

-- | 1.1: Pattern match the second value from a tuple.

second :: (Int, Int) -> Int
second tuple = undefined

-- | second
-- >>> second (1, 2)
-- 2
-- >>> second (2, 3)
-- 3
-- >>> second (4, 10)
-- 10

-- | 1.2: Pattern match the second value from a three-tuple.
secondv2 :: (Int, Int, Int) -> Int
secondv2 tuple = undefined

-- | secondv2
-- >>> secondv2 (1, 2, 3)
-- 2
-- >>> secondv2 (2, 3, 4)
-- 3
-- >>> secondv2 (4, 10, 100)
-- 10

-- | 1.3.1: Return True if the input is Nothing.
isNothing :: Maybe Int -> Bool
isNothing maybeInt = undefined

-- | isNothing
-- >>> isNothing (Just 10)
-- False
-- >>> isNothing Nothing
-- True
-- >>> isNothing (Just 0)
-- False

-- | 1.3.2: Return the Int within the Maybe Int input. If Nothing, return an
-- | error with an appropriate message.
fromJust :: Maybe Int -> Int
fromJust maybeInt = undefined

-- | fromJust
-- >>> fromJust (Just 10)
-- 10
-- >>> fromJust (Just 0)
-- 0

-- | 1.3.3: Use the above function "fromJust" to turn all Maybe Int's into just
-- | the Int component. Nothing's can default to 0.
-- | Don't use a case statement (use guards with isNothing).
maybeIntToInt :: Maybe Int -> Int
maybeIntToInt maybeInt = undefined

-- | maybeIntToInt
-- >>> maybeIntToInt (Just 10)
-- 10
-- >>> maybeIntToInt Nothing
-- 0
-- >>> maybeIntToInt (Just 0)
-- 0

-- | 1.4: For each of the possible "Fruit" inputs, return the length of the word
-- | e.g. Apple is 5, Egg is 3. Use a case statement.

data Fruit = Apple | Banana | Carrot | Dog | Egg | Fork | Grape

fruitLength :: Fruit -> Int
fruitLength fruit = undefined

-- | fruitLength
-- >>> fruitLength Apple
-- 5
-- >>> fruitLength Dog
-- 3
-- >>> fruitLength Grape
-- 5

-- | ========================= Question 2 =============================== | --
-- | GUARDS AND BINARY COMPARISONS

-- | 2.1: Returns True if inputs are identical.
equal :: Int -> Int -> Bool
equal x y = undefined

-- | equal
-- >>> equal 3 7
-- False
-- >>> equal 7 3
-- False
-- >>> equal 49 49
-- True

-- | 2.2: Returns the smallest of two inputs.
smallest :: Int -> Int -> Int
smallest x y = undefined

-- | smallest
-- >>> smallest 3 7
-- 3
-- >>> smallest 7 3
-- 3
-- >>> smallest 49 49
-- 49

-- | 2.3: Returns "first" or "second" depending on which is smaller.
-- | If they are the same, return "equal".
whichSmaller :: Int -> Int -> String
whichSmaller x y = undefined

-- | whichSmaller
-- >>> whichSmaller 3 7
-- "first"
-- >>> whichSmaller 7 3
-- "second"
-- >>> whichSmaller 49 49
-- "equal"

-- | 2.4: Returns True if the first input is smaller.

-- REFERENCE: data Bool = True | False
isSmaller :: Int -> Int -> Bool
isSmaller x y = undefined

-- | isSmaller
-- >>> isSmaller 3 7
-- True
-- >>> isSmaller 7 3
-- False
-- >>> isSmaller 49 49
-- False

-- | 2.5: Returns LT (Less Than) or GT (Greater Than) depending on the first
-- | input relative to the second. If equal, return EQ.

-- REFERENCE: data Ordering = LT | GT | EQ
order :: Int -> Int -> Ordering
order x y = undefined

-- | order
-- >>> order 3 7
-- LT
-- >>> order 7 3
-- GT
-- >>> order 49 49
-- EQ

-- | ========================= Question 3 =============================== | --
-- | TURNING MATH FUNCTIONS INTO CODE: Do some research online for the
-- | mathematical equations for each function.

-- | 3.1: Area of a trapezoid.

trapezoidArea :: Float -> Float -> Float -> Float
trapezoidArea side1 side2 height = undefined

-- | trapezoidArea
-- >>> trapezoidArea 1 1 1
-- 1.0
-- >>> trapezoidArea 1 2 4
-- 6.0
-- >>> trapezoidArea 4 4 8
-- 32.0

-- | 3.2.1: Slope of a line (gradient)
-- | (it may be useful to pattern match out the constituents of the tuple).

slope :: (Float, Float) -> (Float, Float) -> Float
slope p1 p2 = undefined

-- | slope
-- >>> slope ((-1), (-1)) (1, 1)
-- 1.0
-- >>> slope (1, 1) ((-1), (-1))
-- 1.0
-- >>> slope ((-1), 1) (1, (-1))
-- -1.0


-- | 3.2.2: Sometimes the gradient can be undefined. Figure out when this
-- | might be the case. Return "Just <distance>" for sensible results, and
-- | "Nothing" if undefined. See examples below for reference.

slopesafe :: (Float, Float) -> (Float, Float) -> Maybe Float
slopesafe p1 p2 = undefined

-- | slopesafe
-- >>> slopesafe ((-1), (-1)) (1, 1)
-- Just 1.0
-- >>> slopesafe (0, 0) (0, 0)
-- Nothing
-- >>> slopesafe (0, 0) (0, 1)
-- Just Infinity

-- | 3.3: Distance between two points on a 2D plane
-- | (try drawing this on paper, it's just pythagoras with offset).

distance2D :: (Float, Float) -> (Float, Float) -> Float
distance2D p1 p2 = undefined

-- | distance2D
-- >>> distance2D ((-1), (-1)) (2, 3)
-- 5.0
-- >>> distance2D ((-2), (-3)) (1, 1)
-- 5.0
-- >>> distance2D (0, 0) (5, 12)
-- 13.0

-- | ========================= Question 4 =============================== | --
-- | FUNCTIONS ON CUSTOM LIST TYPE: See the below definition for a list of Ints.
-- | Hint: Remember that case statements work well for functions working on
-- | data types with multiple patterns.

data List = Empty | Cons Int List

-- | 4.1: Head - return the first element of a list

listHead :: List -> Int
listHead list = undefined

-- | listHead
-- >>> listHead (Cons 1 (Cons 2 (Cons 3 Empty)))
-- 1
-- >>> listHead (Cons 99 (Cons (-12) Empty))
-- 99
-- >>> listHead (Cons 5 (Cons 4 (Cons 3 (Cons 2 (Cons 1 Empty)))))
-- 5
-- >>> listHead (Cons 1337 Empty)
-- 1337

-- | 4.2: Tail - return all elements of a list excluding the head

listTail :: List -> List
listTail list = undefined

-- | listTail
-- >>> listTail (Cons 1 (Cons 2 (Cons 3 Empty)))
-- Cons 2 (Cons 3 Empty)
-- >>> listTail (Cons 99 (Cons (-12) Empty))
-- Cons (-12) Empty
-- >>> listTail (Cons 5 (Cons 4 (Cons 3 (Cons 2 (Cons 1 Empty)))))
-- Cons 4 (Cons 3 (Cons 2 (Cons 1 Empty)))
-- >>> listTail (Cons 1337 Empty)
-- Empty

-- | 4.3: Last - return the last element of a list

listLast :: List -> Int
listLast list = undefined

-- | listHead
-- >>> listHead (Cons 1 (Cons 2 (Cons 3 Empty)))
-- 3
-- >>> listHead (Cons 99 (Cons (-12) Empty))
-- -12
-- >>> listHead (Cons 5 (Cons 4 (Cons 3 (Cons 2 (Cons 1 Empty)))))
-- 1
-- >>> listHead (Cons 1337 Empty)
-- 1337

-- | 4.4: Init - return all elements of a list excluding the last

listInit :: List -> List
listInit list = undefined

-- | listInit
-- >>> listInit (Cons 1 (Cons 2 (Cons 3 Empty)))
-- Cons 1 (Cons 2 Empty))
-- >>> listInit (Cons 99 (Cons (-12) Empty))
-- Cons 99 Empty
-- >>> listInit (Cons 5 (Cons 4 (Cons 3 (Cons 2 (Cons 1 Empty)))))
-- Cons 5 (Cons 4 (Cons 3 (Cons 2 Empty)))
-- >>> listInit (Cons 1337 Empty)
-- Empty

-- | 4.5: Prepend - add an additional element to the start of a list

listPrepend ::  Int -> List -> List
listPrepend int list = undefined

-- | listPrepend
-- >>> listPrepend 0 (Cons 1 (Cons 2 (Cons 3 Empty)))
-- Cons 0 (Cons 1 (Cons 2 (Cons 3 Empty)))
-- >>> listPrepend 101 (Cons 99 (Cons (-12) Empty))
-- Cons 101 (Cons 99 (Cons (-12) Empty))
-- >>> listPrepend 6 (Cons 5 (Cons 4 (Cons 3 (Cons 2 (Cons 1 Empty)))))
-- Cons 6 (Cons 5 (Cons 4 (Cons 3 (Cons 2 (Cons 1 Empty)))))
-- >>> listPrepend 420 (Cons 1337 Empty)
-- Cons 420 (Cons 1337 Empty)

-- | 4.6: Append - add an additional element to the end of a list

listAppend ::  Int -> List -> List
listAppend int list = undefined

-- | listAppend
-- >>> listAppend 4 (Cons 1 (Cons 2 (Cons 3 Empty)))
-- Cons 1 (Cons 2 (Cons 3 (Cons 4 Empty)))
-- >>> listAppend 101 (Cons 99 (Cons (-12) Empty))
-- Cons 99 (Cons (-12) (Cons 101 Empty))
-- >>> listAppend 0 (Cons 5 (Cons 4 (Cons 3 (Cons 2 (Cons 1 Empty)))))
-- Cons 5 (Cons 4 (Cons 3 (Cons 2 (Cons 1 (Cons 0 Empty)))))
-- >>> listAppend 420 (Cons 1337 Empty)
-- Cons 1337 (Cons 420 Empty)

-- | 4.7: Get - get the element at a given index (the nth element)

listGet :: Int -> List -> Int
listGet index list = undefined

-- | 4.8: Set - set the element at a given index (the nth element)

listSet :: Int -> Int -> List -> List
listSet index item list = undefined

-- | 4.9: Take - return the first n elements of a list

listTake :: Int -> List -> List
listTake index list = undefined

-- | ========================= Question 5 =============================== | --
-- | PROGRAM FLOW: Without running them in GHCi, try to figure out what each
-- | of the following functions do.

-- | 5.1
a :: Integer -> Integer
a x = c x
    where
        b :: Integer -> Integer
        b y = x + y
        c :: Integer -> Integer
        c z = b z

-- Answer: YOUR ANSWER HERE

-- | ========================= Question X =============================== | --
-- | EXTENSION / FUTURE TASKS (come back to these in the later weeks)

-- | X.1: Give each of the functions above a polymorphic type signature
-- | (preferably the most general possible).