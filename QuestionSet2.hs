{-|
Module      : Question Set 2
Author      : Questions written by David Quarel
Purpose     : Questions of similar form and difficulty to the final exam
-}
module QuestionSet2 where

-- | FOR THIS FILE, REFER TO QUESTIONSET2.PDF | --

-- | ========================= Question 1 =============================== | --
-- | GENERAL PROBLEM SOLVING

-- | 1.1.0: Checks if the first string is contained inside the second string.
substring :: String -> String -> Bool
substring = undefined -- TODO

-- Examples
-- >>> substring "abc" "abaca"
-- False
-- >>> substring "cat" "catamaran"
-- True
-- >>> substring "cde" "abcde"
-- True

-- | 1.2.0: Checks if the first list is a permutation (a reordering) of the second list.
shuffle :: (Eq a) => [a] -> [a] -> Bool
shuffle = undefined -- TODO

-- Examples
-- >>>shuffle "hello" "leloh"
-- True
-- >>>shuffle [1,2] [3,2,1]
-- False
-- >>>shuffle [3,1,9] [9,3,1]
-- True
-- >>>shuffle [3,1,9] [9,3,3,1]
-- False

-- | 1.2.1: Shuffle, but can be written faster since there is ordering on type a.
shuffleFast :: (Ord a) => [a] -> [a] -> Bool
shuffleFast = undefined -- TODO

-- Examples
-- >>>shuffleFast "hello" "leloh"
-- True
-- >>>shuffleFast [1,2] [3,2,1]
-- False
-- >>>shuffleFast [3,1,9] [9,3,1]
-- True
-- >>>shuffleFast [3,1,9] [9,3,3,1]
-- False

-- | ========================= Question 2 =============================== | --
-- | 2: Identify what the following functions do. You should explain succintly,
-- | in at most a few lines, what each function does. You should describe for what
-- | inputs, if any, will get the code trapped in an infinite loop, or throw an error.
-- | Do this before testing them in GHCi. See QuestionSet2.pdf for more info.

-- | Question 2.1
foo :: [a] -> a
foo list = case list of
    [x] -> x
    x:y:xs -> foo xs
-- Explanation: TODO

-- | Question 2.2
bar :: Int -> Int -> Int
bar x y
    |x == 0 = y
    |otherwise = bar (x-1) (2*y)
-- Explanation: TODO

-- | Question 2.3
zap :: Int -> Int
zap 0 = 0
zap n = pow (n-2)
    where
        pow 0 = 1
        pow n = zap (n+1)
-- Explanation: TODO

-- | ========================= Question 3 =============================== | --
-- | 3: Recall that the definition of a binary tree is:

data Tree a = Null | Node a (Tree a) (Tree a)
    deriving (Show)

-- | 3.1: Searches a tree for any occourances of the first input x, and replaces
-- | them all with the second input y. See QuestionSet1.pdf for more info.
replace :: (Tree a) -> a -> a -> (Tree a)
replace tree x y = undefined -- TODO

-- Examples
-- >>> replace (Node 1 Null Null) 1 2
-- Node 2 Null Null
-- >>> replace (Node 1 Null Null) 2 17
-- Node 1 Null Null
-- >>> replace (Node 17 Null (Node 3 Null (Node 17 Null Null))) 17 20
-- Node 20 Null (Node 3 Null (Node 20 Null Null))

-- | 3.2: Same thing as replace, but now you may assume that the input trees are
-- | binary search trees. Don't use replace! See QuestionSet1.pdf for more info.
replaceBT :: (Ord a) => (Tree a) -> a -> a -> (Tree a)
replaceBT tree x y = undefined -- TODO

-- | 3.3: Takes the first tree, and checks if it is a subtree of the second tree
subTree :: (Eq a) => (Tree a) -> (Tree a) -> Bool
subTree tree1 tree2 =  undefined -- TODO

-- Examples
-- >>>subTree (Node 3 Null Null) (Node 4 (Node 3 Null Null) (Node 5 Null Null))
-- True
-- >>>subTree (Node 2 Null Null) Null
-- False
-- >>>subTree (Node 4 Null Null) (Node 7 (Node 4 (Node 1 Null Null) Null) (Node 9 Null Em
-- False

-- | 3.3.1: You may need to define a function that checks if two trees are equal.
eqTree :: (Eq a) => (Tree a) -> (Tree a) -> Bool
eqTree tree1 tree2 =  undefined -- TODO

-- | 3.4: subTree, but you can assume both trees are binary search trees.
-- | Don’t use subTree to define subTreeBT, as before.
subTreeBT :: (Ord a) => (Tree a) -> (Tree a) -> Bool
subTreeBT tree1 tree2 =  undefined -- TODO

-- | ========================= Question 4 =============================== | --
-- | 4: What should the type declaration of these functions be? Make the types
-- | as general as possible.

-- | 4.1
-- pup :: -- TODO
pup a b c = (a+b) `elem` c

-- | 4.2
-- bap :: -- TODO
bap a b c = case a of
    Nothing -> b
    (Just j) -> c:j

-- | 4.3
-- yip :: -- TODO
yip a b c = case b of
    Null -> c
    (Node n left right)
        |a == n -> 1 + (yip a left c)
        |otherwise -> yip a left c

-- | ========================= Question 5 =============================== | --
-- | 5: Answer the following questions in at most a paragraph or so of explanation.

-- | 5.1: Give some reasons for and against functions with no side-effects.
-- Explanation:     TODO

-- | 5.2: What does it mean for Haskell to be “strongly typed”?
-- Explanation:     TODO

-- | 5.3: Give some reasons for and against lazy evaluation.
-- Explanation:     TODO

-- | ========================= Question 6 =============================== | --
-- | 6: Give the big-O (worst case) time complexity for the functions we wrote
-- | in Questions 1 and 3. Justify your answer.

-- | 1.1.0:
-- Answer:          TODO
-- Justification:   TODO

-- | 1.2.0:
-- Answer:          TODO
-- Justification:   TODO

-- | 1.2.1:
-- Answer:          TODO
-- Justification:   TODO

-- | 3.1:
-- Answer:          TODO
-- Justification:   TODO

-- | 3.2:
-- Answer:          TODO
-- Justification:   TODO

-- | 3.3:
-- Answer:          TODO
-- Justification:   TODO

-- | 3.3.1:
-- Answer:          TODO
-- Justification:   TODO

-- | 3.4:
-- Answer:          TODO
-- Justification:   TODO