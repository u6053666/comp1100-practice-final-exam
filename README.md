# COMP1130/1100 Final Exam Practice Tasks:

## How to download:
- Fork the project to your account
- Clone the project into IntelliJ using the instructions in lab2 (https://cs.anu.edu.au/courses/comp1100/labs/02/)

## Usage:
- Open files interactively using ghci: `ghci QuestionSet1.hs`
- Test entire files using doctest: `doctest QuestionSet1.hs`
- You may change type signatures for a task if they do not explicitly say not to, but only do so with good reason

## Credit:
- Question Set 1: Created by Joshua Corner with some inspiration from lab tasks
- Question Set 2: Created by David Quarel (adapted and refined by Joshua Corner), also has an attached problem description PDF
- Question Set 3: Created by Joshua Corner with some inspiration from lab tasks as midsem practice questions