{-|
Module      : Question Set 1
Author      : Questions written by Joshua Corner
Credit      : Some inspired by COMP1100 labs
Purpose     : A wide range of questions of similar form and structure to the final exam,
            : inspiring the use of functional decomposition to solve problems
-}
module QuestionSet1 where

import Data.List hiding (subsequences)

-- | ========================= Question 1 =============================== | --
-- | GENERAL PROBLEM SOLVING

-- | 1.1: Return the length of a list (don't use length)
length0f :: [a] -> Int
length0f list = undefined -- TODO

-- | lengthOf
-- >>> length0f []
-- 0
-- >>> length0f [1337]
-- 1
-- >>> length0f [1..10]
-- 10
-- >>> length0f ['z','y'..'a']
-- 26

-- | 1.2.0: Takes a list, and returns it but in reverse, using the (++) operator
reverseSlow :: [a] -> [a]
reverseSlow list = undefined -- TODO

-- | reverseSlow
-- >>> reverseSlow []
-- []
-- >>> reverseSlow [1337]
-- [1337]
-- >>> reverseSlow [1..10]
-- [10,9,8,7,6,5,4,3,2,1]
-- >>> reverseSlow ['z','y'..'v']
-- "vwxyz"

-- | 1.2.1: Takes a list, and returns it but in reverse, using the (:) operator
-- | You will probably need a helper function for this
reverseFast :: [a] -> [a]
reverseFast list = reverseFastHelper list []
    where
        reverseFastHelper :: [a] -> [a] -> [a]
        reverseFastHelper list' accumulator = undefined -- TODO

-- | reverseFast
-- >>> reverseFast []
-- []
-- >>> reverseFast [1337]
-- [1337]
-- >>> reverseFast [1..10]
-- [10,9,8,7,6,5,4,3,2,1]
-- >>> reverseFast ['z','y'..'v']
-- "vwxyz"

-- | 1.3: Return the middle element from a 3-tuple
middle :: (a,b,c) -> b
middle = undefined -- TODO

-- | middle
-- >>> middle (1,3,5)
-- 3
-- >>> middle (30,(20,30),"hello")
-- (20,30)

-- | 1.4: Return the absolute value of a number (don't use abs)
absoluteValue :: (Num a, Ord a) => a -> a
absoluteValue number = undefined -- TODO

-- | absoluteValue
-- >>> absoluteValue 10
-- 10
-- >>> absoluteValue (-13)
-- 13
-- >>> absoluteValue 0
-- 0

-- | 1.5: If both inputs are not 'Nothing' then multiply the values
maybeMultiplication :: (Num a) => (Maybe a) -> (Maybe a) -> (Maybe a)
maybeMultiplication = undefined -- TODO

-- | maybeMultiplication
-- >>> maybeMultiplication Nothing Nothing
-- Nothing
-- >>> maybeMultiplication Nothing (Just 10)
-- Nothing
-- >>> maybeMultiplication (Just 1337) (Just 0)
-- Just 0
-- >>> maybeMultiplication (Just 13) (Just 37)
-- Just 481
-- >>> maybeMultiplication (Just 0) Nothing
-- Nothing

-- | 1.6: Display an arbitrary type as a string for all patterns
-- | You can try enumerating patterns (but there's also a better way!)
data Fruit = Apple | Banana | Carrot | Dog | Egg | Fork | Grape

displayFruit :: Fruit -> String
displayFruit fruit = undefined -- TODO

-- | displayFruit
-- >>> displayFruit Apple
-- "Apple"
-- >>> displayFruit Carrot
-- "Carrot"
-- >>> displayFruit Egg
-- "Egg"

-- | ========================= Question 2 =============================== | --
-- | PROFICIENT USE OF HIGHER ORDER FUNCTIONS (try doing these in 1 line)
-- | These may require the use of map / filter / folds / etc

-- | 2.1: Doubles all numbers in a list
doubleAll :: (Num a) => [a] -> [a]
doubleAll list = undefined -- TODO

-- | doubleAll
-- >>> doubleAll [1,2,3]
-- [2,4,6]
-- >>> doubleAll [1..5]
-- [2,4,6,8,10]
-- >>> doubleAll []
-- []

-- | 2.2: Returns only the positive numbers in a list
onlyPositives :: (Num a, Ord a) => [a] -> [a]
onlyPositives list = undefined -- TODO

-- | onlyPositives
-- >>> onlyPositives [0..10]
-- [0,1,2,3,4,5,6,7,8,9,10]
-- >>> onlyPositives [-10..10]
-- [0,1,2,3,4,5,6,7,8,9,10]
-- >>> onlyPositives []
-- []

-- | 2.3: Adds all elements in a list together
addAll :: (Num a) => [a] -> a
addAll list = undefined -- TODO

-- | addAll
-- >>> addAll [0..10]
-- 55
-- >>> addAll [-10..10]
-- 0
-- >>> addAll []
-- 0

-- | 2.4: Returns True if a number is prime
-- | You can try to do this in one line if you'd like.
-- | https://en.wikipedia.org/wiki/Prime_number
isPrime :: Integer -> Bool
isPrime n = undefined -- TODO

-- | isPrime
-- >>> isPrime 1
-- False
-- >>> isPrime 2
-- True
-- >>> isPrime 4
-- False
-- >>> isPrime 6
-- False
-- >>> isPrime 7
-- True

-- | 2.4.1: Try to make a version of isPrime in as few character as possible. This
-- | is not good coding practice, but it's fun and is an example of code golf
-- | (I've done isPrime in 36 characters without using any helper functions)
p :: Integer -> Bool
p n = undefined -- TODO

-- | isPrime (small)
-- >>> and (zipWith (==) (map isPrime [1..50]) (map p [1..50]))
-- True

-- | ========================= Question 4 =============================== | --
-- | COMMON SORTING ALGORITHMS

-- | 3.1: Sorts a list in ascending order using the quicksort algorithm
-- | https://en.wikipedia.org/wiki/Quicksort
-- | Take the first element, then put all the other elements on the left or right
-- | depending on their value (smaller goes on the left)
quicksort :: (Ord a) => [a] -> [a]
quicksort list = undefined -- TODO

-- | quicksort
-- >>> quicksort "abaca"
-- "aaabc"
-- >>> quicksort [10,9..0]
-- [0,1,2,3,4,5,6,7,8,9,10]
-- >>> quicksort [1337]
-- [1337]
-- >>> quicksort []
-- []

-- | 3.2: Sorts a list in ascending order using the selection sort algorithm
-- | https://en.wikipedia.org/wiki/Selection_sort
-- | Find the smallest element, put it at the beginning, and recurse

-- | 3.2.1: Removes the first occurance of an element in a list
removeFirstOccurrence :: (Eq a) => a -> [a] -> [a]
removeFirstOccurrence element list = undefined -- TODO

-- | removeFirstOccurrence
-- >>> removeFirstOccurrence 'a' "abaca"
-- "baca"
-- >>> removeFirstOccurrence 3 [1..5]
-- [1,2,4,5]
-- >>> removeFirstOccurrence 1337 [1337]
-- []
-- >>> removeFirstOccurrence 'a' []
-- ""

-- | 3.2.2: Selection Sort
selectionsort :: (Ord a) => [a] -> [a]
selectionsort list = undefined -- TODO

-- | selectionsort
-- >>> selectionsort "abaca"
-- "aaabc"
-- >>> selectionsort [10,9..0]
-- [0,1,2,3,4,5,6,7,8,9,10]
-- >>> selectionsort [1337]
-- [1337]
-- >>> selectionsort []
-- []

-- | 3.3: Sorts a list in ascending order using the mergesort algorithm
-- | https://en.wikipedia.org/wiki/Merge_sort
-- | This is a bit harder! Maybe come back to it after the others.
-- | Framework credit to: https://gist.github.com/morae/8494016 (spoilers!)

-- | 3.3.1: Get the first half of a list
firstHalf :: [a] -> [a]
firstHalf xs = undefined -- TODO

-- | 3.3.2: Get the second half of a list
secondHalf :: [a] -> [a]
secondHalf xs = undefined -- TODO

-- | halfExamples
-- >>> firstHalf [1,2,3,4,5]
-- [1,2]
-- >>> secondHalf [1,2,3,4,5]
-- [3,4,5]

-- | 3.3.3: Rules:
-- | 1. Merge of a list with an empty list is just the list
-- | 2. Merge of two lists is the smaller of the two list heads, concatenated with
-- | the result of merging the remainder of that list with all of the other list
merge :: Ord a => [a] -> [a] -> [a]
merge xs ys = undefined -- TODO

-- | merge
-- >>> merge [1,3,7,8] [2,4,5,6]
-- [1,2,3,4,5,6,7,8]

-- | 3.3.4: Merge Sort
mergesort :: (Ord a) => [a] -> [a]
mergesort list = undefined -- TODO

-- | mergesort
-- >>> mergesort "abaca"
-- "aaabc"
-- >>> mergesort [10,9..0]
-- [0,1,2,3,4,5,6,7,8,9,10]
-- >>> mergesort [1337]
-- [1337]
-- >>> mergesort []
-- []

-- | ========================= Question 4 =============================== | --
-- | FIZZBUZZ: https://en.wikipedia.org/wiki/Fizz_buzz

-- | 4.1.1: Rules for number n:
-- | If n is divisible by 3 return "Fizz"
-- | If n is divisible by 5 return "Buzz"
-- | If n is divisible by 3 AND 5 return "FizzBuzz"
-- | If n is none of the above, return n as a string (hint, use the show function)
fizzBuzz :: Int -> String
fizzBuzz n = undefined -- TODO

-- | fizzBuzz
-- >>> fizzBuzz 3
-- "Fizz"
-- >>> fizzBuzz 10
-- "Buzz"
-- >>> fizzBuzz 15
-- "FizzBuzz"
-- >>> fizzBuzz 4
-- "4"

-- | 4.1.2: For 1 to n, return all FizzBuzz output (use fizzBuzz, see examples)
fizzBuzzRecursive :: Int -> [String]
fizzBuzzRecursive n = undefined -- TODO

-- | fizzBuzzRecursive
-- >>> fizzBuzzRecursive 3
-- ["1","2","Fizz"]
-- >>> fizzBuzzRecursive 7
-- ["1","2","Fizz","4","Buzz","Fizz","7"]

-- | 4.1.3: Use fizzBuzzRecursive to return a single string (see examples)
fizzBuzzRecursiveString :: Int -> String
fizzBuzzRecursiveString n = undefined -- TODO

-- | fizzBuzzRecursiveString
-- >>> fizzBuzzRecursiveString 3
-- "1 2 Fizz"
-- >>> fizzBuzzRecursiveString 15
-- "1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz"

-- | ========================= Question 5 =============================== | --
-- | HISTOGRAM: https://en.wikipedia.org/wiki/Histogram

-- | 5.1.1: Return the number of times a certain element appears in a list
frequency :: (Eq a) => a -> [a] -> Int
frequency element list = undefined -- TODO

-- | frequency
-- >>> frequency 'a' "aabaaca"
-- 5
-- >>> frequency 5 [1..10]
-- 1
-- >>> frequency 100 [1..99]
-- 0

-- | 5.1.2: Take an elem and a list, and returns the list without ANY of that elem
-- | This may not be necessary for 5.1.3, though it may help
removeAllElement :: (Eq a) => a -> [a] -> [a]
removeAllElement element list = undefined -- TODO

-- | removeAllElement
-- >>> removeAllElement 'a' "aabaaca"
-- "bc"
-- >>> removeAllElement 5 [1..10]
-- [1,2,3,4,6,7,8,9,10]
-- >>> removeAllElement 5 [1..4]
-- [1,2,3,4]

-- | 5.1.3: Return a list containing tuples, representing each unique element in the
-- | list, and their frequency
histogram :: (Eq a) => [a] -> [(a, Int)]
histogram list = undefined -- TODO

-- | histogram
-- >>> histogram "aabaaca"
-- [('a',5),('b',1),('c',1)]
-- >>> histogram[1..10]
-- [(1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1)]
-- >>> histogram [1337]
-- [(1337,1)]
-- >>> histogram []
-- []

-- | ========================= Question 6 =============================== | --
-- | RUCKSACK: A variation on the knapsack problem https://en.wikipedia.org/wiki/Knapsack_problem

-- | 6.1: Returns all subsequences (subsets) of a list, including itself (all combinations)
subsequences :: [a] -> [[a]]
subsequences list = undefined -- TODO

-- | subsequences
-- >>> subsequences []
-- [[]]
-- >>> sort (subsequences [1..3])
-- [[],[1],[1,2],[1,2,3],[1,3],[2],[2,3],[3]]
-- >>> sort (subsequences ['a'..'d'])
-- ["","a","ab","abc","abcd","abd","ac","acd","ad","b","bc","bcd","bd","c","cd","d"]

-- | 6.2: Does the sum of a list equal the target
sumEqualsTarget :: (Eq a, Num a) => a -> [a] -> Bool
sumEqualsTarget target list = undefined -- TODO

-- | sumEqualsTarget
-- >>> sumEqualsTarget 0 []
-- True
-- >>> sumEqualsTarget 6 [1..3]
-- True
-- >>> sumEqualsTarget 55 [1..10]
-- True

-- | 6.3: Return all subsequences of the list that add up to the target sum
rucksack :: (Eq a, Num a) => [a] -> a -> [[a]]
rucksack list target = undefined -- TODO

-- | rucksack
-- >>> sort (rucksack [3,7,5,9,13,17] 30)
-- [[3,5,9,13],[13,17]]
-- >>> sort (rucksack [3,7,5,9,13,17] 26)
-- [[9,17]]
-- >>> sort (rucksack [3,7,5,9,13,17] 2)
-- []

-- | ========================= Question 7 =============================== | --
-- | NATURALS: Implement a custom number system, inspired by 2017s1 1130 Final Exam
-- | Each Natural is either Zero, or the successor (+1) of another natural number
-- | E.g. 0 is Zero, 2 is Succ (Succ Zero)

data Natural = Zero | Succ (Natural)
    deriving (Show)

-- | 7.1: Turn a Natural into the Haskell type Int (this is very similar to lengthOf
-- | for custom lists)
naturalToInt :: Natural -> Int
naturalToInt nat = undefined -- TODO

-- | 7.2: Turn a Haskell Int into a Natural
intToNatural :: Int -> Natural
intToNatural int = undefined -- TODO

-- | 7.3: Is a Natural zero? Return true if zero
naturalIsZero :: Natural -> Bool
naturalIsZero nat = undefined -- TODO

-- | SATURATED ARITHMETIC: The following problems may require saturated arithmetic.
-- | This means that if a Natural would have below 0, that it equals 0.
-- | Example: (Succ Zero) `subtractNaturals` (Succ (Succ Zero) = Zero
-- | https://en.wikipedia.org/wiki/Saturation_arithmetic

-- | NOTE: For the following questions, do not use +, -, *, /, or other number operations

-- | 7.4.1 Add 1 to a Natural
add1Natural :: Natural -> Natural
add1Natural nat = undefined -- TODO

-- | 7.4.2 Subtract 1 from a Natural (saturated)
sub1Natural :: Natural -> Natural
sub1Natural nat = undefined -- TODO

-- | 7.5: Compare two Naturals
compareNaturals :: Natural -> Natural -> Ordering
compareNaturals nat1 nat2 = undefined -- TODO

-- | 7.5: Add two Naturals,
addNaturals :: Natural -> Natural -> Natural
addNaturals nat1 nat2 = undefined -- TODO

-- | 7.6: Subtract a Natural from another (saturated)
subNaturals :: Natural -> Natural -> Natural
subNaturals nat1 nat2 = undefined -- TODO

-- | 7.7: Multiply two Naturals
mulNaturals :: Natural -> Natural -> Natural
mulNaturals nat1 nat2 = undefined -- TODO

-- | 7.8: Divide and mod Natural by another (saturated)
-- | Should return (quotient, remainder)
-- | This may help http://www-history.mcs.st-and.ac.uk/~john/MT4517/Lectures/L6.html
divModNaturals :: Natural -> Natural -> (Natural, Natural)
divModNaturals nat1 nat2 = undefined -- TODO

divNaturals :: Natural -> Natural -> Natural
divNaturals nat1 nat2 = fst (nat1 `divModNaturals` nat2)

modNaturals :: Natural -> Natural -> Natural
modNaturals nat1 nat2 = snd (nat1 `divModNaturals` nat2)

-- | 7.9: Even and odd (try to define these in terms of each other, using mutual recursion)
naturalIsEven :: Natural -> Bool
naturalIsEven nat = undefined -- TODO

naturalIsOdd :: Natural -> Bool
naturalIsOdd nat = undefined -- TODO

-- | ========================= Question 8 =============================== | --
-- | MYSTERY FUNCTIONS (Credit: Probie): Describe what they do, and their
-- | big-O (worst case) complexity. Try to do this without using ghci
-- | (test it afterwards to check, try giving 24 as input to both)

-- | 8.1.1
biff :: Int -> Int
biff 0 = 1
biff n = biff (n-1) + biff (n-1)
-- What does it do:
-- Complexity:

-- | 8.1.2
baff :: Int -> Int
baff 0 = 1
baff n = boff + boff
    where
        boff = baff (n-1)
-- What does it do:
-- Complexity: